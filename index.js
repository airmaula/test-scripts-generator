var excelJS = require('exceljs')
var fs = require('fs')
const readdir = require("fs").readdir;
const readline = require("readline");
const config = require('config')
const tsgConfig = config.get('tsg.config.js')

const exportTestScripts = async (scripts) => {

    const workbook = new excelJS.Workbook();
    const worksheet = workbook.addWorksheet("Scripts");
    const path = "./tests-scripts";

    if (!fs.existsSync(path)){
        fs.mkdirSync(path);
    }

    worksheet.columns = [
        { header: "Test Condition ID", key: "Test Condition ID", width: 25 },
        { header: "Test Condition Description", key: "Test Condition Description", width: 25 },
        { header: "Test Script ID", key: "Test Script ID", width: 25 },
        { header: "Test Script Description", key: "Test Script Description", width: 25 },
        { header: "Test Data", key: "Test Data", width: 25 },
        { header: "Pre-Condition", key: "Pre-Condition", width: 25 },
        { header: "Steps Name", key: "Steps Name", width: 25 },
    ];

    scripts.forEach(script => {
        worksheet.addRow(script.row);
    })

    console.log(tsgConfig)

    try {
        await workbook.xlsx.writeFile(`${path}/test-scripts.xlsx`)
        console.log('Test scripts generated successful')
    } catch (err) {
        console.log('Test scripts generated failed')
    }

}

const getDirectories = (source, callback) => {
    return readdir(source, { withFileTypes: true }, (err, files) => {
        if (err) {
            callback(err)
        } else {
            callback(
                files
                    .filter(dirent => dirent.isDirectory())
                    .map(dirent => dirent.name)
            )
        }
    })
}

const getFiles = (source, callback) => {
    return readdir(source, { withFileTypes: true }, (err, files) => {
        if (err) {
            callback(err)
        } else {
            callback(
                files
                    .map(dirent => dirent.name)
            )
        }
    })
}

module.exports = () => {
    getDirectories('./cypress/e2e', (test_dirs) => {
        test_dirs.forEach(dir => {
            getFiles('./cypress/e2e/' + dir, (test_scripts) => {
                let scripts = [];
                test_scripts.forEach((test_file, i) => {
                    var myInterface = readline.createInterface({
                        input: fs.createReadStream('./cypress/e2e/' + dir + '/' + test_file)
                    })

                    let row = {};
                    const defs = [
                        'Test Condition ID',
                        'Test Condition Description',
                        'Test Script ID',
                        'Test Script Description',
                        'Test Data',
                        'Pre-Condition',
                        'Steps Name',
                    ];

                    myInterface.on('line', (line) => {
                        defs.forEach(desc => {
                            if (line.search(desc) > -1) {
                                row[desc] = line.split(desc+': ')[1] || ''
                            }
                        })
                    })

                    myInterface.on('close', () => {
                        scripts.push({
                            name: test_file,
                            row,
                        })

                        if (i === test_scripts.length - 1) {
                            exportTestScripts(scripts)
                        }
                    })
                })
            })
        })
    })
}