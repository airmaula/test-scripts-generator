module.exports = {
    tests_directory:  "cypress/e2e/",
    output_directory: "test-scripts",
    fileName: "test-scripts.xlsx",
    columns: [
        "Test Condition ID",
        "Test Condition Description",
        "Test Script ID"
    ]
}